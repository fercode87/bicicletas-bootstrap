var map = L.map('main_map').setView([-34.633993, -58.526948], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' ,{
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
}).addTo(map);


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.Bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });          
              
    }
})